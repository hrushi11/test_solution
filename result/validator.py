from typing import List
from result.exceptions import NumberColumnsValidationError, BaseIdValidationError


def validate(row: List) -> bool:
    if not (len(row) - 1) % 3 == 0:
        raise NumberColumnsValidationError
    if row[0] == '' or row[1] == '' or row[2] == '' or row[3] == '':
        raise BaseIdValidationError
    return True