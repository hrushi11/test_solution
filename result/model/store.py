from typing import List


class Store:
    def __init__(self, id, label: str , link: str , children: List):
        self.label = label
        self.id: str = id
        self.link = link
        self.children = children

    def add_child(self, child):
        self.children.append(child)