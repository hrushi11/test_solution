from typing import List
from result.model.store import Store
from result.helpers import to_find_id
from result.constants import ID_INCREMENT, ID_START, base_groceries

def convert(row: List) -> List:
    grocery: Store = None
    for i in range(ID_START, len(row), ID_INCREMENT):
        prev_grocery = grocery
        grocery = to_find_id(row[i], base_groceries if not grocery else grocery.children)
        if not prev_grocery and not grocery:
            grocery = Store(label=row[i-1], id=row[i], link=row[i+1], children= [])
            base_groceries.append(grocery)
        elif not grocery:
            prev_grocery.add_child(Store(label=row[i-1], id=row[i], link=row[i+1], children=[]))