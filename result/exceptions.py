
class BusinessValidationError(Exception):
    message = 'Business Error'


class BaseIdValidationError(BusinessValidationError):
    message = 'Base ID Validation error - it cannot be empty'


class NumberColumnsValidationError(BusinessValidationError):
    message = 'Number of columns might be extra or missing'

