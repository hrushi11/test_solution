from result.model.store import Store
import json
from typing import List, Union


def to_find_id(grocery_id: str, grocery_list: List) -> Union[Store, None]:
    for grocery in grocery_list:
        if grocery.id == grocery_id:
            return grocery
    else:
        return None


def to_dictionary(obj: object) -> dict:
    if not hasattr(obj,"__dict__"):
        return obj
    result = {}
    for key, val in obj.__dict__.items():
        if key.startswith("_"):
            continue
        element = []
        if isinstance(val, list):
            for item in val:
                element.append(to_dictionary(item))
        else:
            element = to_dictionary(val)
        result[key] = element
    return result


def to_json_writer(path: str, class_list: List) -> List:
    g_json_list = []
    for grocery in class_list:
        g_json_list.append(to_dictionary(grocery))
    with open(path, 'w') as json_file:
        json_file.write(json.dumps(g_json_list))
    return g_json_list
