import csv
import os
import json
from result.exceptions import BusinessValidationError
from result.helpers import to_json_writer
from result.validator import validate
from result.converter import convert
from result.constants import base_groceries, SOURCE_FILE_NAME, TRIGGER_FILE_NAME


DIR_PATH = os.path.dirname(os.path.realpath(__file__))

row_tracker = {'rows_processed_count': 0,
               'rows_success_count': 0,
               'rows_error_count': 0,
               'errors': []}


def process(headers: int = 0):
    try:
        with open(os.path.join(DIR_PATH, SOURCE_FILE_NAME), newline='') as data_file:
            for i in range(0, headers):
                next(data_file)

            for i, row in enumerate(csv.reader(data_file, delimiter=',')):
                try:
                    row_tracker['rows_processed_count'] += 1
                    if validate(row):
                        convert(row)
                        row_tracker['rows_success_count'] += 1
                except BusinessValidationError as e:
                    row_tracker['rows_error_count'] += 1
                    print(e)
                    row_tracker['errors'].append({'row_number': i, 'message': e.message})
                except Exception as e:
                    raise e
        print(json.dumps(to_json_writer(os.path.join(DIR_PATH, TRIGGER_FILE_NAME), base_groceries)))
    except FileNotFoundError as e:
        row_tracker['errors'].append('File not at expected location. Please check')
    except Exception as e:
        row_tracker['errors'].append(e)


if __name__ == "__main__":
    process(headers=1)
    print(row_tracker)
